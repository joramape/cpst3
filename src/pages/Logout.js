import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

export default function Logout(){
	//localStorage.clear()

	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the local storage of the user's information
	unsetUser();

	
	useEffect(() => {
		setUser({
		   id: null,
           isAdmin: null,
           firstName: null,
           lastName: null
		});
	})

	return(
		<Navigate to="/login" />
	)
}