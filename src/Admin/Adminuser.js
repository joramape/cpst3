import { Breadcrumb, Button, Col, Form, InputGroup, Row} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import { Lock } from 'react-bootstrap-icons';
import Swal from 'sweetalert2';

import ViewUser from './Config/ViewUser'





export default function AdminUser() {

 // State hooks to store values of the input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(false)
  
// Add new Admin
  const registerAdmin = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register-admin`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1
      })
    })
    .then(res => res.json())
    .then(data => {

      if(data === true){
        Swal.fire({
          title:"Registration successful",
          icon: "success"
        })
        setFirstName("");
        setLastName("");
        setEmail("");
        setPassword1("");
        setPassword2("");
      } else {
        Swal.fire({
          title:"Your Email is Already Exist!",
          icon: "error",
          text: "Please try Again!"
        })
        setFirstName("");
        setLastName("");
        setEmail("");
        setPassword1("");
        setPassword2("");
      }
    })
  };

  useEffect(() => {

    // Validation to enable submit button
    if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "") && (password1 === password2) && (password1.length && password2 >=8)) {
        setIsActive(true);
    } else {
      setIsActive(false)
    }

  

  },[email, password1, password2, firstName, lastName])
 
  return (
     <Row className="mt-5">
        <Breadcrumb>
          <h2>Add Admin User</h2>
        </Breadcrumb>
       <hr />
     {/* Add new Admin */}
        <Form onSubmit={registerAdmin}>
          <Row className="mb-3">
            <Form.Group as={Col} md="4" controlId="validationFormik01">
              <Form.Label>First name</Form.Label>
              <Form.Control 
                    type="text" 
                    placeholder="Enter First Name"
                    value = {firstName}
                    onChange = {e => setFirstName(e.target.value)} 
                    required
                  /> 
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationFormik02">
              <Form.Label>Last name</Form.Label>
             <Form.Control 
                  type="text" 
                  placeholder="Enter Last Name"
                  value = {lastName}
                  onChange = {e => setLastName(e.target.value)} 
                  required
                />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            
          </Row>
          <Row className="mb-3">
           <Form.Group as={Col} md="4" controlId="validationFormikUsername">
              <Form.Label>Username</Form.Label>
              <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange = {e => setEmail(e.target.value)} 
                    required
                  />
                <Form.Control.Feedback type="invalid">
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>

             <Form.Group as={Col} md="4" controlId="validationFormikUsername">
              <Form.Label>Password</Form.Label>
              <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend"><Lock /></InputGroup.Text>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                  />
                <Form.Control.Feedback type="invalid">
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
             <Form.Group as={Col} md="4" controlId="validationFormikUsername">
              <Form.Label>Verify Password</Form.Label>
              <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend"><Lock /></InputGroup.Text>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value = {password2}
                    onChange = {e => setPassword2(e.target.value)}
                    required
                  />
                <Form.Control.Feedback type="invalid">
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
           
          </Row>
          { isActive ?
            <Button type="submit">Submit</Button>
            :
            <Button type="submit" disabled>Submit</Button>
          }
          
        </Form>

      {/* View all User*/}
        <hr className="mt-5" />
         <ViewUser />
      </Row>
  );
}
